static const char col_gray1[]       = "#878787";	// bg
static const char col_gray2[]       = "#000000";	// inactive win border
static const char col_gray3[]       = "#000000";	// font color
static const char col_gray4[]       = "#ffffff";	// current tag/win font color
static const char col_cyan[]        = "#ae81e7";	// bar second color/active win border color
